<html>
<head>
<title>PowerTAC Tournament Manager</title>
<style>
html, body {
    height: 100%;
}
.main {
    height: 100%;
    width: 100%;
    display: table;
}
.wrapper {
    display: table-cell;
    height: 100%;
    vertical-align: middle;
}
</style>
</head>
<body>
<?php
session_start();
include("conf.php");
 
echo '<b>Menu:</b> <a href="createtournament.php">Create a Tournament</a> | <a href="index.php">List Games</a><br><br>';
 
echo '<form name="form1" method="post" action="">
<strong>This will delete existing games and create a new list!</strong><br />
<input type="text" name="prefix" value="sim"  style="width:25%"> Prefix for log files. ex: sim<br>
<textarea name="brokers" rows="14"  style="width:25%">AgentUDE16,Spot16,TacTex15,Maxon15</textarea> Comma seperated broker list, no whitespace. ex: AgentUDEXP,crocodileAgent<br>
<input type="text" name="gamesize" value="2"  style="width:25%"> Game size (int). Comma separeted for multiple sizes. ex: 3,5<br>
<input type="text" name="multiplier" value="2"  style="width:25%"> # of multiplier (int). Comma separeted corresponds to game size. ex: 2,2.<br>
<input type="text" name="mustbroker" value="AgentUDEXP"  style="width:25%"> Set if a broker must always included in all games. It must be excluded from the broker list above.<br>
<br />
<input type="submit" name="createTournamentButton" value="Create a Tournament"/>
</form>';
 
 
if($_POST['createTournamentButton']!="")
{
    $prefix = trim($_POST['prefix']);
    $brokers = trim($_POST['brokers']);
    $gamesize = trim($_POST['gamesize']).",";
    $multiplier = trim($_POST['multiplier']).",";
	$mustbroker = trim($_POST['mustbroker']);
    $brokersarray = explode(",", $brokers);
    $brokercount = sizeof($brokersarray);
    echo "<h3>Broker count ".$brokercount.", game size ".$gamesize.", multiplier: ".$multiplier.", mustbroker: ".$mustbroker."</h3>";
	
	mysql_query("Delete from tournament where 1");
	mysql_query("Delete from games where 1");
	
	mysql_query("insert into tournament values (NULL, '$prefix', '$brokers', '$gamesize', '$multiplier', '$mustbroker', '', '')") or die(mysql_error());
	$tournamentId = mysql_insert_id();
    
	if($mustbroker !="")
		$mustbroker = $mustbroker.",";
	else
		$mustbroker = "";
     
	
	$gamesizeA = explode(",", $gamesize);
	$multiplierA = explode(",", $multiplier);
	$countS = 0;
	$gamenumber = 0;
	while($gamesizeA[$countS])
	{
		$newgamesize = $gamesizeA[$countS];
		for($i = 0; $i < $multiplierA[$countS]; $i++)
		{
			if($brokercount < $newgamesize)
			{
				$gamenumber++;
				$brokerset = substr($mustbroker.$brokers, 0, strlen($mustbroker.$brokers) - 1);
				echo "Game-".$gamenumber.": ". $brokerset;
				mysql_query("insert into games values ($gamenumber, $tournamentId, '$brokerset', '$prefix', '', '')");
				echo "<br>";
			}
			else
			{
				foreach(new Combinations($brokersarray, $newgamesize) as $substring){
					$gamenumber++;
					$brokerset = $mustbroker;
					foreach($substring as $brk)
						$brokerset .= $brk.",";
					$brokerset = substr($brokerset, 0, strlen($brokerset) - 1);
					mysql_query("insert into games values ($gamenumber, $tournamentId, '$brokerset', '$prefix', '', '')");
					echo "Game-".$gamenumber.": ".$brokerset."<br>";
				}
			}
		}
		$countS++;
	}
     
}
?>
 
</body>
</html>
<?php
ini_set('mysql.connect_timeout','0'); 
ini_set('default_socket_timeout', 0);
ini_set('max_execution_time', 0);
set_time_limit(0);
ignore_user_abort(true);
session_start();
include("conf.php");

echo "################################### I am ".shell_exec('whoami')." ###################################<br>\r\n";
$rSettings = mysql_fetch_assoc(mysql_query("select * from settings where id=1"));
$qt = mysql_query("select * from tournament order by id ASC");
$rt = mysql_fetch_assoc($qt);
if($rt['end'] != "")
{
	echo "There is no tournament to run!";
}
else if($rt['start'] == "")
{
	echo "The tournament has not started yet!";
}
else
{
	$qnum = mysql_query("select * from games where start='' AND end='' order by id ASC");
	$pendinggames = mysql_num_rows($qnum);
	echo "There are ".$pendinggames." games pending...<br>\r\n";
	
	for($i = 0; $i < $pendinggames; $i++)
	{
		// check if an active game exists
		$qactive = mysql_query("select * from games where start!='' AND end='' order by id ASC limit 1");
		if(mysql_num_rows($qactive) > 0) {
			echo "There is an active game!";
			break;
		}
	
		// check tournament end
		$qt = mysql_query("select * from tournament order by id ASC");
		$rt = mysql_fetch_assoc($qt);
		if($rt['end'] != "")
			break;
		
		try 
		{
			$q = mysql_query("select * from games where start='' AND end='' order by id ASC limit 1");
			if(mysql_num_rows($q) == 0)	break;	
			$r = mysql_fetch_assoc($q);
			$tournamentId = $r['tournamentId'];
			$gameId = $r['id'];
			$brokers = $r['brokers'];
			$prefix = $r['prefix'];
			$start = $r['start'];
			$end = $r['end'];
			
			$datee = date("d.m.Y H:i");
			mysql_query("Update games set start='$datee' where id=".$gameId);
			
			echo "Starting game-".$gameId.": ". $brokers."...<br>\r\n";
				
			$cwd = $rSettings['powertac_path'];
			$mvn_path = $rSettings['mvn_path'];
			$env = NULL;
			$proc=proc_open('sudo '.$mvn_path.' -Pcli -Dexec.args="--sim --boot-data boot.xml --brokers '.$brokers.' --log-suffix '.$prefix.'-'.$gameId.'"',
			  array(
				array("pipe","r"),
				array("pipe","w"),
				array("pipe","w")
			  ),
			  $pipes, $cwd, $env);
			
			
			$count = 10;
			echo "Waiting for logs of Game-".$gameId."...<br>\r\n";
			$content = str_replace(array("\r", "\r\n", "\n"), "<br>", stream_get_contents($pipes[1]));
			echo "Logs received...<br>\r\n";
			fclose($pipes[0]);
			fclose($pipes[1]);
			fclose($pipes[2]);
			$exit_status = proc_close($proc);
			echo "Proc closed...<br>\r\n";
			
			// Lets make sure that the connection is alive.
			mysql_close($mysqlbaglan);
			connectMysql();


			$datee = date("d.m.Y H:i");
			mysql_query("Update games set end='$datee' where id=".$gameId) or die(mysql_error());
			echo "Exit status (process: ".$proc."): ".$exit_status."\r\n";
			$content = "<html><body>".$datee." Exit status (process: ".$proc."): ".$exit_status."<br><br>".$content."</body></html>";
			file_put_contents("logs/".$prefix."-".$gameId.".html", $content);
			
		}
		catch(Exception $e) { echo "Exception Message: ".$e->getMessage(); }
		
		echo "Game-".$gameId." completed...<br>\r\n";
	}
		
}
?>
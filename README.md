Power TAC Tournament Manager 10/02/2020, Serkan Ozdemir, serkan.ozdemir@uni-due.de

Power TAC Tournament Manager (PTM) is an easy-to-use simple PHP-based tool that schedules and manages multiple games between brokers that are running in the same machine. PTM provides a graphical interface to create game variations and lead the tournament. PTM provides game-specific information during the tournament. All interactions are saved in log files.

*****************************

## Download PTM:

PTM is publicly accessible on https://bitbucket.org/serkan-ozdemir/power-tac-tournament-manager. You need a git client to download the project files:

git clone git@bitbucket.org:serkan-ozdemir/power-tac-tournament-manager.git
This command creates a directory called power-tac-tournament-manager and various subdirectories.

*****************************

## Requirements:

* Web Server (Apache suggested)
* A Linux-based operating system
* MySQL DBMS
* A MySQL DBMS client, preferably phpMyAdmin
* PHP 5.4+
* Apache Maven 3.5.2+
* Power TAC Server Distribution

************************

## Installation:

1. Import "powertac_tournament_manager.sql" to your database using a MySQL client.
2. Edit "conf.php" to add your database authorization details.
3. Set a cron job for "startgame.php" so that it can be executed once every minute.
4. Using your web browser, navigate to "index.php".
5. Set Apache Maven path in the settings area.
6. Set Power TAC Server Distribution path in the settings area (can be downloaded from https://github.com/powertac/server-distribution). 

************************

## Usage:

1. All broker instances must be running locally. They must be configured to be able to communicate with the local Power TAC distribution.
2. Using your web browser, navigate to "index.php".
3. Click "Create a Tournament".
4. Fill out the form, depending on your experiment needs. 
5. Click the "List Games".
6. Click the "Start" button to initiate the tournament.
7. Click the "Stop" button to abort the tournament.
8. The tournament manager will save its logs under the "logs" folder. Power TAC simulator saves its state log files under its relevant folders. 

-- phpMyAdmin SQL Dump
-- version 4.0.10.1
-- http://www.phpmyadmin.net
--
-- Anamakine: localhost
-- Üretim Zamanı: 11 Şub 2020, 13:18:31
-- Sunucu sürümü: 5.5.60-MariaDB
-- PHP Sürümü: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Veritabanı: `zadmin_pla`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `games`
--

CREATE TABLE IF NOT EXISTS `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tournamentId` int(11) NOT NULL,
  `brokers` varchar(255) NOT NULL,
  `prefix` varchar(255) NOT NULL,
  `start` varchar(255) NOT NULL,
  `end` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Tablo döküm verisi `games`
--

INSERT INTO `games` (`id`, `tournamentId`, `brokers`, `prefix`, `start`, `end`) VALUES
(1, 27, 'AgentUDEXP,AgentUDE16,Spot16', 'sim', '', ''),
(2, 27, 'AgentUDEXP,AgentUDE16,TacTex15', 'sim', '', ''),
(3, 27, 'AgentUDEXP,AgentUDE16,Maxon15', 'sim', '', ''),
(4, 27, 'AgentUDEXP,AgentUDE16,ben', 'sim', '', ''),
(5, 27, 'AgentUDEXP,AgentUDE16,ten', 'sim', '', ''),
(6, 27, 'AgentUDEXP,Spot16,TacTex15', 'sim', '', ''),
(7, 27, 'AgentUDEXP,Spot16,Maxon15', 'sim', '', ''),
(8, 27, 'AgentUDEXP,Spot16,ben', 'sim', '', ''),
(9, 27, 'AgentUDEXP,Spot16,ten', 'sim', '', ''),
(10, 27, 'AgentUDEXP,TacTex15,Maxon15', 'sim', '', ''),
(11, 27, 'AgentUDEXP,TacTex15,ben', 'sim', '', ''),
(12, 27, 'AgentUDEXP,TacTex15,ten', 'sim', '', ''),
(13, 27, 'AgentUDEXP,Maxon15,ben', 'sim', '', ''),
(14, 27, 'AgentUDEXP,Maxon15,ten', 'sim', '', ''),
(15, 27, 'AgentUDEXP,ben,ten', 'sim', '', ''),
(16, 27, 'AgentUDEXP,AgentUDE16,Spot16', 'sim', '', ''),
(17, 27, 'AgentUDEXP,AgentUDE16,TacTex15', 'sim', '', ''),
(18, 27, 'AgentUDEXP,AgentUDE16,Maxon15', 'sim', '', ''),
(19, 27, 'AgentUDEXP,AgentUDE16,ben', 'sim', '', ''),
(20, 27, 'AgentUDEXP,AgentUDE16,ten', 'sim', '', ''),
(21, 27, 'AgentUDEXP,Spot16,TacTex15', 'sim', '', ''),
(22, 27, 'AgentUDEXP,Spot16,Maxon15', 'sim', '', ''),
(23, 27, 'AgentUDEXP,Spot16,ben', 'sim', '', ''),
(24, 27, 'AgentUDEXP,Spot16,ten', 'sim', '', ''),
(25, 27, 'AgentUDEXP,TacTex15,Maxon15', 'sim', '', ''),
(26, 27, 'AgentUDEXP,TacTex15,ben', 'sim', '', ''),
(27, 27, 'AgentUDEXP,TacTex15,ten', 'sim', '', ''),
(28, 27, 'AgentUDEXP,Maxon15,ben', 'sim', '', ''),
(29, 27, 'AgentUDEXP,Maxon15,ten', 'sim', '', ''),
(30, 27, 'AgentUDEXP,ben,ten', 'sim', '', '');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mvn_path` varchar(255) NOT NULL,
  `powertac_path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Tablo döküm verisi `settings`
--

INSERT INTO `settings` (`id`, `mvn_path`, `powertac_path`) VALUES
(1, '/var/installed/apache-maven-3.3.1/bin/mvn', '/var/installed/powertac-distribution');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `tournament`
--

CREATE TABLE IF NOT EXISTS `tournament` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prefix` varchar(255) NOT NULL,
  `brokerList` varchar(255) NOT NULL,
  `sizes` varchar(255) NOT NULL,
  `multiplier` varchar(255) NOT NULL,
  `mustbroker` varchar(255) NOT NULL,
  `start` varchar(255) NOT NULL,
  `end` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Tablo döküm verisi `tournament`
--

INSERT INTO `tournament` (`id`, `prefix`, `brokerList`, `sizes`, `multiplier`, `mustbroker`, `start`, `end`) VALUES
(27, 'sim', 'AgentUDE16,Spot16,TacTex15,Maxon15,ben,ten', '2,', '2,', 'AgentUDEXP', '', '');
